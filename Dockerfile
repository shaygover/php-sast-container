# v1.4.0 05.07.2023
# Build with: docker build -t phpcs --build-arg EXECUTION=400 --build-arg MEMORY=1024M --build-arg IMPUT_TIME=-1 .
# To force build without cache: docker-compose build --no-cache
# docker-compose up --build -d
# Find with docker container ls
# Attach with: docker exec -ti my_phpcs /bin/bash
FROM archlinux:base-devel
LABEL Version="1.4.0"
LABEL Name="phpcs"

# php Env vars. That's the syntax. Don't ask me why.
# See https://docs.docker.com/engine/reference/builder/#using-arg-variables
ARG EXECUTION=${EXECUTION:-500}
ARG MEMORY=${MEMORY:-2048M}
ARG IMPUT_TIME=${IMPUT_TIME:-300}

# Change pacaman settings
RUN sed -i 's/#Color/Color/' /etc/pacman.conf
RUN sed -i 's/NoProgressBar/#NoProgressBar/' /etc/pacman.conf

# Add non root user
RUN useradd -m notroot && echo "notroot ALL=(ALL:ALL) NOPASSWD:ALL" > /etc/sudoers.d/notroot

# Install all that can be installed with pacman
RUN pacman -Syu --noconfirm && pacman -S git php go composer yara --noconfirm

# Build yay
WORKDIR /home/notroot
USER notroot
RUN git clone https://aur.archlinux.org/yay.git 
WORKDIR /home/notroot/yay
RUN makepkg -si --noconfirm

# Install packages from AUR
RUN yay -S php-codesniffer --noconfirm

# Back to root
USER root

# Remove unneeded folder
WORKDIR /home/notroot/
RUN rm -rf yay/

# Setting ground
RUN mkdir /phpcs
RUN mkdir /sec
WORKDIR /phpcs/

# Version compitability
RUN git clone --branch 9.3.5 https://github.com/PHPCompatibility/PHPCompatibility.git
RUN git clone --branch 2.1.4 https://github.com/PHPCompatibility/PHPCompatibilityWP.git
RUN git clone --branch 1.3.2 https://github.com/PHPCompatibility/PHPCompatibilityParagonie.git

# Composer installations. Installed to /home/notroot/.config/composer/vendor/bin
USER notroot
RUN composer global config --no-interaction allow-plugins.phpstan/extension-installer true
RUN composer global require --no-interaction phpstan/phpstan --prefer-dist
RUN composer global require --no-interaction phpstan/extension-installer
RUN composer global require --no-interaction szepeviktor/phpstan-wordpress
# RUN composer global require --no-interaction vimeo/psalm - psalm must be installed into specific project due to bug https://github.com/vimeo/psalm/issues/4025
RUN composer global require --no-interaction scr34m/php-malware-scanner
RUN composer global require --no-interaction designsecurity/progpilot
RUN composer clear-cache

# PHPStan config
RUN mkdir /home/notroot/phpstan
COPY --chown=notroot phpstan7.4.conf.neon phpstan8.conf.neon /home/notroot/phpstan/

# phpcs Security
USER root
RUN git clone https://github.com/FloeDesignTechnologies/phpcs-security-audit.git

# Other tools
WORKDIR /sec/
RUN git clone https://github.com/jvoisin/php-malware-finder.git
WORKDIR /sec/php-malware-finder/
RUN make


# Config phpcs
RUN phpcs --config-set installed_paths /phpcs/PHPCompatibility/,/phpcs/phpcs-security-audit/Security,/phpcs/PHPCompatibilityWP/PHPCompatibilityWP,/phpcs/PHPCompatibilityParagonie/PHPCompatibilityParagonieSodiumCompat,/phpcs/PHPCompatibilityParagonie/PHPCompatibilityParagonieRandomCompat

# PHP config. 
RUN sed -i "s/max_execution_time = /max_execution_time = ${EXECUTION} ;/g" /etc/php/php.ini 
RUN sed -i "s/max_input_time = /max_input_time = ${IMPUT_TIME} ;/g" /etc/php/php.ini
RUN sed -i "s/memory_limit = /memory_limit = ${MEMORY} ;/g" /etc/php/php.ini

# Prepare input vol
RUN mkdir /data && chown notroot /data -R
VOLUME ["/data"]

# Run settings
USER notroot
WORKDIR /data

# Add aliases:
RUN echo 'alias phpsec="phpcs --colors -p . --extensions=php,inc,lib,module,info --standard=Security"' >> ~/.bashrc && \
    echo 'alias phpwpver7.4="phpcs --colors -p . --standard=PHPCompatibilityWP --runtime-set testVersion 7.4"' >> ~/.bashrc && \
    echo 'alias phpver7.4="phpcs --colors -p . --standard=PHPCompatibility --runtime-set testVersion 7.4"' >> ~/.bashrc && \
    echo 'alias phpstan7.4="/home/notroot/.config/composer/vendor/bin/phpstan analyze -c /home/notroot/phpstan/phpstan7.4.conf.neon ."' >> ~/.bashrc && \
    echo 'alias phpstan8="/home/notroot/.config/composer/vendor/bin/phpstan analyze -c /home/notroot/phpstan/phpstan8.conf.neon ."' >> ~/.bashrc && \
    echo 'alias phpmalwarescanner="/home/notroot/.config/composer/vendor/bin/scan -Esxbp"' >> ~/.bashrc && \
    echo 'alias phpwpver8.0="phpcs --colors -p . --standard=PHPCompatibilityWP --runtime-set testVersion 8.0"' >> ~/.bashrc && \
    echo 'alias phpwpver8.1="phpcs --colors -p . --standard=PHPCompatibilityWP --runtime-set testVersion 8.1"' >> ~/.bashrc && \
    echo 'alias phpwpver8.2="phpcs --colors -p . --standard=PHPCompatibilityWP --runtime-set testVersion 8.2"' >> ~/.bashrc && \
    echo 'alias phpver8.0="phpcs --colors -p . --standard=PHPCompatibility --runtime-set testVersion 8.0"' >> ~/.bashrc && \
    echo 'alias phpver8.1="phpcs --colors -p . --standard=PHPCompatibility --runtime-set testVersion 8.1"' >> ~/.bashrc && \
    echo 'alias phpver8.2="phpcs --colors -p . --standard=PHPCompatibility --runtime-set testVersion 8.2"' >> ~/.bashrc && \
    echo 'alias progpilot="/home/notroot/.config/composer/vendor/bin/progpilot ."' >> ~/.bashrc && \
    echo 'alias phpmalwarefinder="/sec/php-malware-finder/php-malware-finder ."' >> ~/.bashrc
