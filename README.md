# php-sast-container

Made for Ort Israel R&D.

Docker container with PHP SAST and version compatibility tools.

Currently supports the following checks:

* phpcs - installed via arch AUR.
    * (Version compatibility) [PHPCompatibility](https://github.com/PHPCompatibility/PHPCompatibility) v9.3.5
    * (Version compatibility for WordPress) [PHPCompatibilityWP](https://github.com/PHPCompatibility/PHPCompatibilityWP) v2.1.4
    * (Version compatibility for WordPress) [PHPCompatibilityParagonie](https://github.com/PHPCompatibility/PHPCompatibilityParagonie) v1.3.2. Required by PHPCompatibilityWP.
    * (SAST) [phpcs-security-audit](https://github.com/FloeDesignTechnologies) latest.
* (Version compatibility) phpstan - installed via composer.
    * Configured for version checks only (with run level 0).
    * Configuration files included and will be compied to container on build. Use them to change run level and other parameters: https://phpstan.org/config-reference. YAML conf files.
    * Installed extensions: Wordpress. Need more? Open an issue.
* (SAST) php-malware-finder - cloned from git.
    * https://github.com/jvoisin/php-malware-finder
* (SAST) php-malware-scanner - cloned from git.
    * https://github.com/scr34m/php-malware-scanner
* (SAST) progpilot - installed via composer.
    * https://github.com/designsecurity/progpilot

## Aliases

* phpcs (PHP 8+ support is incomplete):
    * **phpsec** for phpcs --colors -p . --extensions=php,inc,lib,module,info --standard=Security
    * **phpwpver7.4** for phpcs --colors -p . --standard=PHPCompatibility --runtime-set testVersion 7.4
    * **phpver7.4** for phpcs --colors -p . --standard=PHPCompatibilityWP --runtime-set testVersion 7.4
    * **phpwpver8.0** for phpcs --colors -p . --standard=PHPCompatibility --runtime-set testVersion 8.0
    * **phpver8.0** for phpcs --colors -p . --standard=PHPCompatibilityWP --runtime-set testVersion 8.0
    * **phpwpver8.1** for phpcs --colors -p . --standard=PHPCompatibility --runtime-set testVersion 8.1
    * **phpver8.1** for phpcs --colors -p . --standard=PHPCompatibilityWP --runtime-set testVersion 8.1
    * **phpwpver8.2** for phpcs --colors -p . --standard=PHPCompatibility --runtime-set testVersion 8.2
    * **phpver8.2** for phpcs --colors -p . --standard=PHPCompatibilityWP --runtime-set testVersion 8.2
* phpstan:
    * **phpstan8** for /home/notroot/.config/composer/vendor/bin/phpstan analyze -c /home/notroot/phpstan/phpstan8.conf.neon .
    * **phpstan7.4** for /home/notroot/.config/composer/vendor/bin/phpstan analyze -c /home/notroot/phpstan/phpstan7.4.conf.neon .
* php-malware-finder:
    * **phpmalwarefinder** for /sec/php-malware-finder/php-malware-finder . (Add -v for debug info).
* php-malware-scanner:
    * **phpmalwarescanner** for /home/notroot/.config/composer/vendor/bin/scan -Esxbp. E for scanning files without .php extension (sometimes php code hides insied js files), s to continue scanning after first hit, x for extara checks, b for base64 ecoded chars (simple way to hide code in gibrish) and p to show the pattern that was matched
* progpilot:
    * **phpmalwarescanner** for /home/notroot/.config/composer/vendor/bin/progpilot .
    * Use -v/-vv/-vvv for more info. Output is json formatted.

## Requirements:
1. Linux: Docker and docker compose.
2. Windows: Docker for windows, powershell and [configuring docker for windows to use bind mounts](https://rominirani.com/docker-on-windows-mounting-host-directories-d96f3f056a2c).

## Build and Run:
1. Edit docker-compose.yml with a path of your choosing and ENV vars. This path is where you should place the php code you want to scan. Available ENV vars:

    1. [MEMORY](https://www.php.net/manual/en/ini.core.php#ini.memory-limit ): Should be set according to your pc free ram. The default 2048M assumes 16GB+ PC RAM. For 8GB use 1024M. **Note**: phpcs tend to be memory hungry.
    2. [EXECUTION](https://www.php.net/manual/en/info.configuration.php#ini.max-execution-time): Max running time in seconds. Default: 500 seconds.
    3. [IMPUT_TIME](https://www.php.net/manual/en/info.configuration.php#ini.max-input-time): Max time in seconds the scan will read your code. Default: 300 seconds.
    **Note:** The last 2 allow you to avoid infinite loops.

3. Run `docker-compose up --build -d`
4. Find container id with `docker container ls`
5. Get tty inside the container with `docker exec -ti <container id> /bin/bash`

**Note:** The container runs with non root user. Should you need root, use sudo.

